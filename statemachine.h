/* 
 * File:   statemachine.h
 * Author: jgr
 *
 * Created on 22 de Janeiro de 2020, 14:08
 */

#ifndef STATEMACHINE_H
#define	STATEMACHINE_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* STATEMACHINE_H */

extern unsigned char dinstatus_anterior[16];
extern unsigned char doutstatus_blink[16];
extern unsigned char doutstatus_blink_out[16];

//Different state of ATM machine

#define MAX_ACONTECIMENTOS 1000
eSystemEvent acontecimentos[MAX_ACONTECIMENTOS];
int acontecimentosEmEspera = 0;

extern int repeteSubida;

eSystemEvent read_event(void) {
    int i;
    eSystemEvent res = NOP_Event;
    if (acontecimentosEmEspera > 0) {
        res = acontecimentos[0];
        for (i = 0; i < acontecimentosEmEspera; i++) {
            acontecimentos[i] = acontecimentos[i + 1];
        }
        acontecimentosEmEspera--;
        // printf("read_event %s\n", nomesEventos[res]);
    }
    return res;
}

//typedef of function pointer
typedef eSystemState(*pfEventHandler)(void);
//structure of state and event with event handler

typedef struct {
    eSystemState eStateMachine;
    eSystemEvent eStateMachineEvent;
    pfEventHandler pfStateMachineEvnentHandler;
} sStateMachine;

typedef struct {
    int motor;
    int subir;
    int sobeumpouco;
    int descer;
    int puxar;
    int empurrar;
    int esperat1;
    int esperat2;
    int esperat3;
    int esperat4;
    int esperat5;
    int esperat6;
    int standby;
    int buzzer;
} timeout;

timeout countdown = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void ProcessaMaquinaEstados(void);

eSystemState onSTART_Handler(void) {
    if (dinstatus_anterior[onSTART_Event]) {
        if (!dinstatus_anterior[onMOTPROT_Event]  && dinstatus_anterior[onSWALEFT_Event] && dinstatus_anterior[onSWARIGHT_Event]) {
            // printf("onSTART_Handler: %d %d %d\n", dinstatus_anterior[onMOTPROT_Event], dinstatus_anterior[onSWALEFT_Event], dinstatus_anterior[onSWARIGHT_Event]);
            motor(1);
            countdown.motor = 80;
            return MotorLigado_State;
        } else {
            // printf("onSTART_Handler fail\n");
            return Alarme_State;
        }
    }
}

eSystemState onEsperaPeloMotor_Handler(void) {
    // printf("onEsperaPeloMotor_Handler - countdown.motor = %d\n", countdown.motor);
    if (countdown.motor > 0) {
        countdown.motor = countdown.motor - 1;
        return MotorLigado_State;
    } else {
        countdown.motor = 0;
        if (!dinstatus_anterior[onSWTOP_Event]) {
            desce(0); // redundante desce(0) porque dava problemas numa maquina usada
            sobe(1);
            countdown.subir = 66;
            // countdown.sobeumpouco = 200; // ao fim de 200 ms, come�a a puxar o tabuleiro
            return Subir_State;
        } else {
            if (!dinstatus_anterior[onSWRIGHT_Event]) {
                countdown.puxar = 120;
                puxa(1);
                return Puxar_State;
            } else {
                entraPronta();
                countdown.standby = 2500;
                return Pronta_State;
            }
        }
    }
}

eSystemState onTickASubir_Handler(void) {
    return Subir_State;
}

eSystemState onTickAPuxar_Handler(void) {
    if (countdown.puxar > 0) {
        countdown.puxar = countdown.puxar - 1;
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
    return Puxar_State;
}

eSystemState onTickAPuxar2_Handler(void) {
    /* if (countdown.subir > 0) {
        countdown.subir = countdown.subir - 1;
    } else {
        buzzerCtrl(ON);
    } */
    if (countdown.puxar > 0) {
        countdown.puxar = countdown.puxar - 1;
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
    return SubidaEsquerda_State;
}

eSystemState onTickAEmpurrar2_Handler(void) {
    /* if (countdown.subir > 0) {
        countdown.subir = countdown.subir - 1;
    } else {
        buzzerCtrl(ON);
    }*/
    if (countdown.empurrar > 0) {
        countdown.empurrar = countdown.empurrar - 1;
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
    return SubidaDireita_State;
}

eSystemState onTickADescer2_Handler(void) {
    if (countdown.descer > 0) {
        countdown.descer = countdown.descer - 1;
        return Descida2_State;
    } else {
        if ( repeteSubida < 2 ) {
            desce(0);
            // Nao entra em alarme
            // Sobe novamente e desce
            repeteSubida = repeteSubida + 1;
            sobe(1);
            countdown.subir = 66;
            return SobeNovamente2_State;
        } else {
            // buzzerCtrl(ON);
            // countdown.buzzer = 195;
            // entraAlarme();
            // return Alarme_State;
            desce(0);
            repeteSubida = 0;
            sobe(1);
            countdown.subir = 66;       
            countdown.esperat5 = 50;
            return InicioSubida2_State;
        }
    }
}

eSystemState onTickADescer_Handler(void) {
    if (countdown.descer > 0) {
        countdown.descer = countdown.descer - 1;
        return Descida_State;
    } else {
        if ( repeteSubida < 2 ) {
            desce(0);
            // Nao entra em alarme
            // Sobe novamente e desce
            repeteSubida = repeteSubida + 1;
            sobe(1);
            countdown.subir = 66;
            return SobeNovamente1_State;
        } else {
            // buzzerCtrl(ON);
            // countdown.buzzer = 195;
            // entraAlarme();
            // return Alarme_State;
            desce(0);
            repeteSubida = 0;
            sobe(1);
            countdown.subir = 66;
            countdown.esperat2 = 50;
            return InicioSubida_State;            
        }
    }
}

eSystemState onInicialTick_Handler(void) {
    if (!dinstatus_anterior[onEMERG_Event]) {
        entraEmergencia();
        return Emergencia_State;
    } else {
        // if (dinstatus_anterior[onSWTOP_Event] && dinstatus_anterior[onSWLOW_Event] &&
        //         dinstatus_anterior[onSWRIGHT_Event] &&
        //         dinstatus_anterior[onMOTPROT_Event] &&
        //         dinstatus_anterior[onSWALEFT_Event] && dinstatus_anterior[onSWARIGHT_Event]) {
        //     entraPronta();
        //     countdown.standby = 2500;
        //     return Pronta_State;
        // } else {
            countdown.buzzer = 195;
            buzzerCtrl(ON);
            entraAlarme();
            return Alarme_State;
        // }
    }
}

eSystemState onEMERG_Handler(void) {
    if (dinstatus_anterior[onEMERG_Event]) {
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    } else {
        entraEmergencia();
        return Emergencia_State;
    }
}

eSystemState onMOTPROT_Handler(void) {
    // if (dinstatus_anterior[onMOTPROT_Event]) {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    // }
}

eSystemState onSWTOP_Handler(void) {
    if (dinstatus_anterior[onSWTOP_Event]) {
        buzzerCtrl(OFF);
        sobe(0);
        countdown.subir = 0;
        // Devia mandar puxar se estamos na inicializacao...
        if (!dinstatus_anterior[onSWRIGHT_Event]) {
            countdown.puxar = 120;
            puxa(1);
            return Puxar_State;
        } else {
            entraPronta();
            countdown.standby = 2500;
            return Pronta_State;
        }
        // return Puxar_State;
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
}

eSystemState onSWRIGHT_Handler(void) {
    if (dinstatus_anterior[onSWRIGHT_Event]) {
        buzzerCtrl(OFF);
        puxa(0);
        countdown.puxar = 0;
        entraPronta();
        countdown.standby = 2500;
        return Pronta_State;
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
}

eSystemState onTickIgnore_Handler(void) {
    return eNextState;
}

eSystemState onTickAlarme_Handler(void) {
    if (countdown.buzzer > 0) {
        countdown.buzzer = countdown.buzzer - 1;
    } else {
        buzzerCtrl(OFF);
    }
    return eNextState;
}

eSystemState onSTARTIgnore_Handler(void) {
    return eNextState;
}

eSystemState onDescida_Handler(void) {
    if (dinstatus_anterior[onSTART_Event]) {
        if (!dinstatus_anterior[onMOTPROT_Event] &&
                dinstatus_anterior[onSWFLEFT_Event] && dinstatus_anterior[onSWFRIGHT_Event] &&
                dinstatus_anterior[onSWALEFT_Event] && dinstatus_anterior[onSWARIGHT_Event]) {
            // printf("onSTART_Handler\n");
            sobe(0); // redundante
            desce(1);
            countdown.descer = 156;
            return Descida_State;
        }
    }
}

eSystemState onSWLOW_Handler(void) {
    if (!dinstatus_anterior[onSWLOW_Event]) {
        buzzerCtrl(OFF);
        desce(0);
        countdown.descer = 0;
        countdown.esperat1 = 80;
        repeteSubida = 0;
        return EsperaT1_State;
    } else {
        if ( repeteSubida < 2 ) {
            desce(0);
            // Nao entra em alarme
            // Sobe novamente e desce
            repeteSubida = repeteSubida + 1;
            sobe(1);
            countdown.subir = 66;
            return SobeNovamente1_State;
        } else {
            // countdown.buzzer = 195;
            // buzzerCtrl(ON);
            // entraAlarme();
            // return Alarme_State; 
            desce(0);
            repeteSubida = 0;
            sobe(1);
            countdown.subir = 66;
            countdown.esperat2 = 50;
            return InicioSubida_State;               
        }
    }
}

eSystemState onSWLOW2_Handler(void) {
    if (!dinstatus_anterior[onSWLOW_Event]) {
        buzzerCtrl(OFF);
        desce(0);
        countdown.descer = 0;
        countdown.esperat4 = 80;
        return EsperaT4_State;
    } else {
        if ( repeteSubida < 2 ) {
            desce(0);
            // Nao entra em alarme
            // Sobe novamente e desce
            repeteSubida = repeteSubida + 1;
            sobe(1);
            countdown.subir = 66;
            return SobeNovamente2_State;
        } else {
            // countdown.buzzer = 195;
            // buzzerCtrl(ON);
            // entraAlarme();
            // return Alarme_State; 
            desce(0);
            repeteSubida = 0;
            sobe(1);
            countdown.subir = 66;       
            countdown.esperat5 = 50;
            return InicioSubida2_State;            
        }
    }
}

eSystemState onTickEsperaT1_Handler(void) {
    // printf("onTickEsperaT1_Handler - countdown.esperat1 = %d\n", countdown.esperat1);
    if (countdown.esperat1 > 0) {
        countdown.esperat1 = countdown.esperat1 - 1;
        return EsperaT1_State;
    } else {
        countdown.esperat1 = 0;
        sobe(1);
        countdown.subir = 66;
        countdown.esperat2 = 50;
        return InicioSubida_State;
    }
}

eSystemState onTickEsperaT4_Handler(void) {
    // printf("onTickEsperaT4_Handler - countdown.esperat4 = %d\n", countdown.esperat4);
    if (countdown.esperat4 > 0) {
        countdown.esperat4 = countdown.esperat4 - 1;
        return EsperaT4_State;
    } else {
        countdown.esperat4 = 0;
        sobe(1);
        countdown.subir = 66;
        countdown.esperat5 = 50;
        return InicioSubida2_State;
    }
}

eSystemState onTickEsperaT2_Handler(void) {
    // printf("onTickEsperaT2_Handler - countdown.esperat2 = %d\n", countdown.esperat2);
    if (countdown.subir > 0) {
        countdown.subir = countdown.subir - 1;
    } /* else {
        buzzerCtrl(ON);
    } */
    if (countdown.esperat2 > 0) {
        countdown.esperat2 = countdown.esperat2 - 1;
        return InicioSubida_State;
    } else {
        // countdown.empurrar pode ter sido j� iniciado pelo SWTOPEmpurra
        if (!countdown.empurrar) {
            countdown.empurrar = 150;
        }
        empurra(1);
        return SubidaDireita_State;
    }
}

eSystemState onTickEsperaT5_Handler(void) {
    // printf("onTickEsperaT5_Handler - countdown.esperat5 = %d\n", countdown.esperat5);
    if (countdown.subir > 0) {
        countdown.subir = countdown.subir - 1;
    } /* else {
        buzzerCtrl(ON);
    } */
    if (countdown.esperat5 > 0) {
        countdown.esperat5 = countdown.esperat5 - 1;
        return InicioSubida2_State;
    } else {
        if (!countdown.puxar) {
            countdown.puxar = 120;
        }
        puxa(1);
        return SubidaEsquerda_State;
    }
}

eSystemState onTickSobeNovamente1_Handler(void) {
    if (countdown.subir > 0) {
        countdown.subir = countdown.subir - 1;
        return SobeNovamente1_State;
    } else {
        countdown.subir = 0;
        sobe(0); // redundante
        desce(1);
        countdown.descer = 156;
        return Descida_State;        
    }
}

eSystemState onTickSobeNovamente2_Handler(void) {
    if (countdown.subir > 0) {
        countdown.subir = countdown.subir - 1;
        return SobeNovamente2_State;
    } else {
        countdown.subir = 0;
        sobe(0); // redundante
        desce(1);
        countdown.descer = 156;
        return Descida2_State;        
    }
}

eSystemState onSWTOPDesce1_Handler(void) {
    if (dinstatus_anterior[onSWTOP_Event]) {
        sobe(0);
        countdown.subir = 0;
        desce(1);
        countdown.descer = 156;
        return Descida_State;  
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
}

eSystemState onSWTOPDesce2_Handler(void) {
    if (dinstatus_anterior[onSWTOP_Event]) {
        sobe(0);
        countdown.subir = 0;
        desce(1);
        countdown.descer = 156;
        return Descida2_State;  
    } else {
        buzzerCtrl(ON);
        countdown.buzzer = 195;
        entraAlarme();
        return Alarme_State;
    }
}

eSystemState onSWTOPSubida_Handler(void) {
    if (dinstatus_anterior[onSWTOP_Event]) {
        buzzerCtrl(OFF);
        sobe(0);
        countdown.subir = 0;
        return eNextState;
    }
}

eSystemState onSWTOPEmpurra_Handler(void) {
    if (dinstatus_anterior[onSWTOP_Event]) {
        buzzerCtrl(OFF);
        sobe(0);
        countdown.subir = 0;
        // countdown.empurrar pode ter sido iniciado pelo EsperaT2
        if (!countdown.empurrar) {
            countdown.empurrar = 150;
        }
        empurra(1);
        return SubidaDireita_State;
    }
}

eSystemState onSWTOPPuxa_Handler(void) {
    if (dinstatus_anterior[onSWTOP_Event]) {
        buzzerCtrl(OFF);
        sobe(0);
        countdown.subir = 0;
        if (!countdown.puxar) {
            countdown.puxar = 120;
        }
        puxa(1);
        return SubidaEsquerda_State;
    }
}

eSystemState onSWLEFT_Handler(void) {
    if (dinstatus_anterior[onSWLEFT_Event]) {
        buzzerCtrl(OFF);
        empurra(0);
        countdown.empurrar = 0;
        if (!dinstatus_anterior[onAUT_Event]) {
            countdown.esperat3 = 80;
        } else {
            countdown.esperat3 = 0;
        }
        return EsperaT3_State;
    }
}

eSystemState onSWRIGHT2_Handler(void) {
    if (dinstatus_anterior[onSWRIGHT_Event]) {
        buzzerCtrl(OFF);
        puxa(0);
        countdown.puxar = 0;
        if (!dinstatus_anterior[onAUT_Event]) {
            countdown.esperat6 = 80;
        } else {
            countdown.esperat6 = 0;
        }
        return EsperaT6_State;
    }
}

eSystemState onTickEsperaT3_Handler(void) {
    // printf("onTickEsperaT3_Handler - countdown.esperat3 = %d\n", countdown.esperat3);
    if (countdown.esperat3 > 0) {
        countdown.esperat3 = countdown.esperat3 - 1;
        return EsperaT3_State;
    } else {
        countdown.esperat3 = 0;
        if (!dinstatus_anterior[onMOTPROT_Event] &&
                dinstatus_anterior[onSWFLEFT_Event] && dinstatus_anterior[onSWFRIGHT_Event] &&
                dinstatus_anterior[onSWALEFT_Event] && dinstatus_anterior[onSWARIGHT_Event]) {
            desce(1);
            countdown.descer = 156;
            return Descida2_State;
        }
    }
}

eSystemState onTickEsperaT6_Handler(void) {
    // printf("onTickEsperaT6_Handler - countdown.esperat6 = %d\n", countdown.esperat6);
    if (countdown.esperat6 > 0) {
        countdown.esperat6 = countdown.esperat6 - 1;
        return EsperaT6_State;
    } else {
        countdown.esperat6 = 0;
        if (STOP) {
            STOP = 0;
            countdown.standby = 2500;
            return Pronta_State;
        } else {
            if (!dinstatus_anterior[onMOTPROT_Event] &&
                    dinstatus_anterior[onSWFLEFT_Event] && dinstatus_anterior[onSWFRIGHT_Event] &&
                    dinstatus_anterior[onSWALEFT_Event] && dinstatus_anterior[onSWARIGHT_Event]) {
                desce(1);
                countdown.descer = 156;
                return Descida_State;
            }            
        }
    }
}

eSystemState onTickStandBy_Handler(void) {
    // printf("onTickStandBy_Handler - countdown.standby = %d\n", countdown.standby);
    if (countdown.standby > 0) {
        countdown.standby = countdown.standby - 1;
        return Pronta_State;
    } else {
        countdown.standby = 0;
        motor(0);
        countdown.buzzer = 195;
        buzzerCtrl(ON);
        entraAlarme();
        return Alarme_State;
    }
}


sStateMachine asStateMachine[] = {
    {Inicial_State, onTick_Event, onInicialTick_Handler},
    // {Emergencia_State, onMOTPROT_Event, onMOTPROT_Handler},
    {Emergencia_State, onEMERG_Event, onEMERG_Handler},
    {Emergencia_State, onTick_Event, onTickIgnore_Handler},
     {Alarme_State, onMOTPROT_Event, onMOTPROT_Handler},
    {Alarme_State, onEMERG_Event, onEMERG_Handler},
    {Alarme_State, onSTART_Event, onSTART_Handler},
    {Alarme_State, onTick_Event, onTickAlarme_Handler},
    {MotorLigado_State, onTick_Event, onEsperaPeloMotor_Handler},
    {MotorLigado_State, onSTART_Event, onSTARTIgnore_Handler},
    // {MotorLigado_State, onMOTPROT_Event, onMOTPROT_Handler},
    {Subir_State, onSWTOP_Event, onSWTOP_Handler},
    {Subir_State, onTick_Event, onTickASubir_Handler},
     {Subir_State, onMOTPROT_Event, onMOTPROT_Handler},

    {Puxar_State, onSWRIGHT_Event, onSWRIGHT_Handler},
    {Puxar_State, onSWLEFT_Event, onTickIgnore_Handler},
    {Puxar_State, onTick_Event, onTickAPuxar_Handler},
    {Puxar_State, onEMERG_Event, onEMERG_Handler},
     {Puxar_State, onMOTPROT_Event, onMOTPROT_Handler},

    {Pronta_State, onSTART_Event, onDescida_Handler},
    {Pronta_State, onTick_Event, onTickStandBy_Handler},
    {Pronta_State, onEMERG_Event, onEMERG_Handler},
     {Pronta_State, onMOTPROT_Event, onMOTPROT_Handler},

    {Descida_State, onSWLOW_Event, onSWLOW_Handler},        // improved
    {Descida_State, onTick_Event, onTickADescer_Handler},   // improved
    {Descida_State, onEMERG_Event, onEMERG_Handler},
     {Descida_State, onMOTPROT_Event, onMOTPROT_Handler},

    {EsperaT1_State, onTick_Event, onTickEsperaT1_Handler},
    {EsperaT1_State, onEMERG_Event, onEMERG_Handler},
     {EsperaT1_State, onMOTPROT_Event, onMOTPROT_Handler},
    
    {SobeNovamente1_State, onTick_Event, onTickSobeNovamente1_Handler}, // done
    {SobeNovamente1_State, onSWTOP_Event, onSWTOPDesce1_Handler},       // done
    {SobeNovamente1_State, onEMERG_Event, onEMERG_Handler},
     {SobeNovamente1_State, onMOTPROT_Event, onMOTPROT_Handler},    

    {InicioSubida_State, onTick_Event, onTickEsperaT2_Handler},
    {InicioSubida_State, onSWTOP_Event, onSWTOPEmpurra_Handler},
    {InicioSubida_State, onEMERG_Event, onEMERG_Handler},
     {InicioSubida_State, onMOTPROT_Event, onMOTPROT_Handler},

    {SubidaDireita_State, onSWTOP_Event, onSWTOPSubida_Handler},
    {SubidaDireita_State, onSWLEFT_Event, onSWLEFT_Handler},
    {SubidaDireita_State, onSWRIGHT_Event, onTickIgnore_Handler},
    // {SubidaDireita_State, onTick_Event, onTickIgnore_Handler},
    {SubidaDireita_State, onTick_Event, onTickAEmpurrar2_Handler},  
    {SubidaDireita_State, onEMERG_Event, onEMERG_Handler},
     {SubidaDireita_State, onMOTPROT_Event, onMOTPROT_Handler},

    {EsperaT3_State, onTick_Event, onTickEsperaT3_Handler},
    {EsperaT3_State, onEMERG_Event, onEMERG_Handler},
     {EsperaT3_State, onMOTPROT_Event, onMOTPROT_Handler},

    {Descida2_State, onSWLOW_Event, onSWLOW2_Handler},      // done
    {Descida2_State, onTick_Event, onTickADescer2_Handler},
    {Descida2_State, onEMERG_Event, onEMERG_Handler},
     {Descida2_State, onMOTPROT_Event, onMOTPROT_Handler},
    
    {EsperaT4_State, onTick_Event, onTickEsperaT4_Handler},
    {EsperaT4_State, onEMERG_Event, onEMERG_Handler},
     {EsperaT4_State, onMOTPROT_Event, onMOTPROT_Handler},

    {SobeNovamente2_State, onTick_Event, onTickSobeNovamente2_Handler}, // done
    {SobeNovamente2_State, onSWTOP_Event, onSWTOPDesce2_Handler},       // done
    {SobeNovamente2_State, onEMERG_Event, onEMERG_Handler},
     {SobeNovamente2_State, onMOTPROT_Event, onMOTPROT_Handler},        
    
    {InicioSubida2_State, onTick_Event, onTickEsperaT5_Handler},
    {InicioSubida2_State, onSWTOP_Event, onSWTOPPuxa_Handler},
    {InicioSubida2_State, onEMERG_Event, onEMERG_Handler},
     {InicioSubida2_State, onMOTPROT_Event, onMOTPROT_Handler},

    {SubidaEsquerda_State, onSWTOP_Event, onSWTOPSubida_Handler},
    {SubidaEsquerda_State, onSWLEFT_Event, onTickIgnore_Handler},
    {SubidaEsquerda_State, onSWRIGHT_Event, onSWRIGHT2_Handler},
    // {SubidaEsquerda_State, onTick_Event, onTickIgnore_Handler},
    {SubidaEsquerda_State, onTick_Event, onTickAPuxar2_Handler},
    {SubidaEsquerda_State, onEMERG_Event, onEMERG_Handler},
     {SubidaEsquerda_State, onMOTPROT_Event, onMOTPROT_Handler},

    {EsperaT6_State, onTick_Event, onTickEsperaT6_Handler},
    {EsperaT6_State, onEMERG_Event, onEMERG_Handler},
     {EsperaT6_State, onMOTPROT_Event, onMOTPROT_Handler}

};
 